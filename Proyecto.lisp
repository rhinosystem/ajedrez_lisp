(make-array '(8 8) :initial-element 0)
(setf *j* #2A((bT bC bA bD bR bA bC bT) (bP bP bP bP bP bP bP bP) (0 0 0 0 0 0 0 0) (0 0 0 0 0 0 0 0) (0 0 0 0 0 0 0 0) (0 0 0 0 0 0 0 0) (nP nP nP nP nP nP nP nP) (nT nC nA nD nR nA nC nT)))
(setf *temp* #2A((TU1 CU1 AU1 DU1 RU1 AU1 CU1 TU1) (PU1 PU1 PU1 PU1 PU1 PU1 PU1 PU1) (0 0 0 0 0 0 0 0) (0 0 0 0 0 0 0 0) (0 0 0 0 0 0 0 0) (0 0 0 0 0 0 0 0) (PU2 PU2 PU2 PU2 PU2 PU2 PU2 PU2) (TU2 CU2 AU2 DU2 RU2 AU2 CU2 TU2)))

;;(print '((aref *j* 0 0)) (aref *j* 0 0))
(defun ImprimirTablero()
 
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 0 0) (aref *j* 0 1) (aref *j* 0 2) (aref *j* 0 3) (aref *j* 0 4) (aref *j* 0 5) (aref *j* 0 6) (aref *j* 0 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 1 0) (aref *j* 1 1) (aref *j* 1 2) (aref *j* 1 3) (aref *j* 1 4) (aref *j* 1 5) (aref *j* 1 6) (aref *j* 1 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 2 0) (aref *j* 2 1) (aref *j* 2 2) (aref *j* 2 3) (aref *j* 2 4) (aref *j* 2 5) (aref *j* 2 6) (aref *j* 2 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 3 0) (aref *j* 3 1) (aref *j* 3 2) (aref *j* 3 3) (aref *j* 3 4) (aref *j* 3 5) (aref *j* 3 6) (aref *j* 3 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 4 0) (aref *j* 4 1) (aref *j* 4 2) (aref *j* 4 3) (aref *j* 4 4) (aref *j* 4 5) (aref *j* 4 6) (aref *j* 4 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 5 0) (aref *j* 5 1) (aref *j* 5 2) (aref *j* 5 3) (aref *j* 5 4) (aref *j* 5 5) (aref *j* 5 6) (aref *j* 5 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 6 0) (aref *j* 6 1) (aref *j* 6 2) (aref *j* 6 3) (aref *j* 6 4) (aref *j* 6 5) (aref *j* 6 6) (aref *j* 6 7))
 (format t "~%~T~T~T~T~T~T~T~T~T~T~S ~S ~S ~S ~S ~S ~S ~S" (aref *j* 7 0) (aref *j* 7 1) (aref *j* 7 2) (aref *j* 7 3) (aref *j* 7 4) (aref *j* 7 5) (aref *j* 7 6) (aref *j* 7 7))
)
(defun clearTablero()
	(loop for x from 1 to 20 do (format t "~%")) 
)
(defun espaciosTablero()
	(loop for x from 1 to 10 do (format t "~%")) 
)
(defun TableroTemp()
	(loop for xtemp from 0 to 7 do (format t "~%") (loop for ytemp from 0 to 7 do (format t "~S~T" (aref *temp* xtemp ytemp))) ) 
)
(defun encabezadoTablero()
	(loop for x from 1 to 101 do (format t "*") )
	(format t "~%")
	(loop for x from 1 to 40 do (format t "*") )
	(format t "~T Juego Ajedrez ~T~T~T~T~T")
	(loop for x from 1 to 40 do (format t "*") )
	(format t "~%")
	(loop for x from 1 to 40 do (format t "*") 	)
	(format t "~T Desarrollado por: ~T")
	(loop for x from 1 to 40 do (format t "*") )
	(format t "~%")
	(loop for x from 1 to 40 do (format t "*") 	)
	(format t "~T Gustavo Londa ~T~T~T~T~T")
	(loop for x from 1 to 40 do (format t "*") )
	(format t "~%")
	(loop for x from 1 to 40 do (format t "*") 	)
	(format t "~T Ronny Morán ~T~T~T~T~T~T~T")
	(loop for x from 1 to 40 do (format t "*") )
	(format t "~%")
	(loop for x from 1 to 101 do (format t "*") )
)
(clearTablero)
(encabezadoTablero)
(espaciosTablero)
(ImprimirTablero)
(espaciosTablero)
;;(TableroTemp)
(setq validarWhile 0)
(loop while (= validarWhile 0) do 
	(terpri)
    (princ "Jugador1 seleccione la pieza: ")
   (setq pU1 (read))
           (if (or (or (equal 'bT pU1) (equal 'bC pU1)) 
           		   (or (equal 'bA pU1) (equal 'bD pU1))) 
           (setq validarWhile 1))
           (if (or (equal 'bR pU1) (equal 'bP pU1)) 	    
           		(setq validarWhile 1))

           (if (or (or (equal 'nT pU1) (equal 'nC pU1)) 
           		   (or (equal 'nA pU1) (equal 'nD pU1))) 
           (setq validarWhile 1))
           (if (or (equal 'nR pU1) (equal 'nP pU1)) 	    
           		(setq validarWhile 1))

	
	
	)

 (terpri)
   (princ "Jugador1 seleccione fila: ")
   (setq fU1 (read))
  (terpri)
   (princ "Jugador1 seleccione la columna: ")
   (setq cU1 (read))
  ;; (loop for x from 0 to 7  do (loop for y from 0 to 7 do 
   	;;(if (equal (aref *j* x y) pU1) (format t "~%~S ~S" x y))) ) 
(if (equal (aref *j* fU1 cU1) pU1)  

	(progn 
	 	(terpri)
   		(princ "Jugador1 ingrese nueva fila: ")
   		(setq fnU1 (read))
  		(terpri)
   		(princ "Jugador1 ingrese nueva columna: ")
   		(setq cnU1 (read))

   		(setf (aref *j* fnU1 cnU1) pU1)
 
	 )   
	 (format t "En la fila ~S y columna ~S no se encuentra la pieza ~S" fU1 cU1 pU1)          
  ;this is optional, if there is an else, then
)
;;(setf (aref *j* fU1 cU1) pU1)
(clearTablero)
(ImprimirTablero)
(clearTablero)